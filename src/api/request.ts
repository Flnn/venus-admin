import Axios, {AxiosInstance, AxiosRequestConfig} from 'axios'

let config: AxiosRequestConfig = {
    baseURL: "http://localhost:3001/venus-admin-api",
    timeout: 3000,
    headers: {
        'content-type': 'application/json'
    }
}
const requestClient: AxiosInstance = Axios.create(config)

requestClient.interceptors.request.use(config => {
    // 请求拦截
    return config;
}, error => {
    console.log('axios request error!', error)
    return Promise.reject(error);
})

requestClient.interceptors.response.use(response => {
    // 响应拦截
    return response;
}, error => {
    console.log('axios response error!', error)
    return Promise.reject(error);
})

export default requestClient;