import request from '../request'

export function getUserList(params: any): Promise<any>{
    return request.post('/sys/user/userList', params)
}