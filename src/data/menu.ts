export interface Menu {
    menuName: string;
    menuIcon: string;
    hasSubMenu: boolean;
    subMenu?: Array<Menu>;
    menuLevel: number;
    index: number;
}

const firstMenu: Menu = {
    menuName: "仪表盘",
    menuIcon: "dashboard",
    hasSubMenu: true,
    menuLevel: 1,
    index: 1,
    subMenu: [{menuName: "工作台",menuLevel:2,menuIcon: "work", hasSubMenu:false, index: 1},
        {menuName: "分析页",menuLevel:2,menuIcon: "analysis", hasSubMenu:false, index: 2}]
}

const formMenu: Menu = {
    menuName: "表单页",
    menuIcon: "forms",
    hasSubMenu: true,
    menuLevel: 1,
    index:2,
    subMenu: [{menuName: "基础表单",menuLevel:2,menuIcon: "Form", hasSubMenu:false,index: 1},
        {menuName: "分步表单", menuLevel:2, menuIcon: "StepForm", hasSubMenu: false,index: 2},
        {menuName: "高级表单", menuLevel:2, menuIcon: "advancedForm", hasSubMenu: false,index: 3}]
}

const listMenu: Menu ={
    menuName: "列表页",
    menuIcon: "list",
    hasSubMenu: true,
    menuLevel: 1,
    index: 3,
    subMenu: [{menuName: "查询列表",menuLevel:2,menuIcon: "queryList", hasSubMenu:false,index: 1},
        {menuName: "卡片列表",menuLevel:2,menuIcon: "cardList", hasSubMenu:false,index: 2},
        {menuName: "搜索列表",menuLevel:2,menuIcon: "searchList", hasSubMenu:true,index: 3, subMenu:
                [{menuName: "文章搜索",menuLevel:3,menuIcon: "searchArticle", hasSubMenu:false,index: 1},
                    {menuName: "项目搜索",menuLevel:3,menuIcon: "searchProject", hasSubMenu:false,index: 2}]}]
}

const detailMenu: Menu = {
    menuName: "详情页",
    menuIcon: "detail",
    hasSubMenu: false,
    menuLevel: 1,
    index: 4
}

const resultMenu: Menu = {
    menuName: "结果页",
    menuIcon: "result",
    hasSubMenu: false,
    menuLevel: 1,
    index: 5
}

const menuList: Array<Menu> = [firstMenu, formMenu, listMenu, detailMenu, resultMenu]

export default menuList;
