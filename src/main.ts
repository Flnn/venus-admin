import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import './styles/app.css'
import 'element-plus/dist/index.css'
import router from './router/index'
import store from './store/index'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const app = createApp(App)

//注册所有element-plus icons组件
for(const [key, component] of Object.entries(ElementPlusIconsVue)){
    app.component(key, component)
}
app.use(router)
    .use(store)
    .use(ElementPlus)
    .mount('#app')
