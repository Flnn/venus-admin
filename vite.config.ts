import { defineConfig,loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { join } from 'path'
//在服务端获取配置参数
const console = require("console")
console.log(loadEnv('develop', './viteEnv'))

const process = require("process")
console.log(process.env.NODE_ENV)
// https://vitejs.dev/config/
export default defineConfig({
  resolve:{
    alias: {
      '@':join(__dirname, "src"),
    }
  },
  envDir: "./viteEnv",
  envPrefix: ["VITE", "VENUS"],
  server:{
    port: 3001,
    strictPort: true,
    /*配置跨域转发*/
    proxy: {
      '/venus-admin-api': {
        target: 'http://localhost:8101/',  //API服务地址
        changeOrigin: true            //开启跨域
      }
    }
  },
  plugins: [vue(), vueJsx()]
})
